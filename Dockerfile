FROM alpine:edge AS builder

WORKDIR /tmp/crystal
RUN apk add --update --no-cache crystal shards g++ gc-dev libc-dev libevent-dev libxml2-dev llvm llvm-dev llvm-libs llvm-static make libressl libressl-dev pcre-dev readline-dev yaml-dev zlib-dev libpq
COPY src/ ./src
COPY shard.yml .
RUN shards build --production
RUN strip -S bin/msts-beetle

FROM alpine:edge
RUN apk add --update libgcc libressl libevent gc pcre dumb-init
WORKDIR /bin
COPY --from=builder /tmp/crystal/bin/* /bin/
