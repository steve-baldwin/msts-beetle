require "beetle"
require "./msts-beetle/**"

s = Beetle::Server.new
s.register_task BCaaS::ChargeTask
s.register_task BCaaS::PreAuthTask
s.register_task B2BC::InvoiceTask
s.register_task B2BC::PreAuthTask
s.register_task B2BC::PreAuthInvoiceTask
s.listen
