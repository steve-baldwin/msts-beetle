module BCaaS
  extend self

  class Client
    property currencies : Array(String)
    property seller : String
    property buyers : Array(String)

    def initialize(@currencies, @seller, @buyers)
    end
  end

  HOST    = "https://app.bcaas-staging.msts.com/api/v20180807"
  API_KEY = {
    "sc" => "239dc441-5855-4b6a-ab2c-5cff41893de2",
    "mc" => "743a2d51-5723-4199-9bef-b0d77811a986",
  }
  CLIENTS = {
    "sc" => Client.new(
      ["USD"],
      "2988265c-1c5f-440c-a29e-65d83bdc2d02",
      ["29ca6af0-abd6-4705-a562-c99dfe92639f", "a84e018b-4a3e-448e-b5fb-8aab9cd661e7"]
    ),
    "mc" => Client.new(
      ["USD", "SGD"],
      "597f6fea-c4f3-4e31-89a2-72e641bab055",
      [
        "3d0b826f-265c-476a-880a-ca5ed80cd25d",
        "2197194e-94d2-4eda-8d85-b49f545d7188",
        "7be6ac84-cfd0-42a3-8b3a-e1f7d2bba9bf",
        "561284d6-e381-4faa-9355-567ee40b2d70",
        "7dae92fb-de22-4bae-99c8-14dc8e6684b3",
        "db058a9c-81e0-467c-bc0b-15fed8dfd0e3",
        "45672a98-281a-4ee3-857a-89ea27fa2dcb",
        "074d3f06-6d3b-480d-b08c-d59bf3c3342a",
        "2b4489cc-e883-47a4-b6d4-ea9921694f44",
        "dafb1c87-ba26-4763-8e65-d89192df5464",
        "5066112c-b321-40a0-ac61-a7868b1921ee",
        "044216f2-5e77-46ac-a580-3a3719ec23c2",
        "bf487f35-589b-4b20-bcf7-97d178c850da",
        "8f87bdc4-68e4-41a6-8ee1-54f3a3f06ad1",
      ]
    ),
  }

  def api_key(sc_mc)
    API_KEY[sc_mc]
  end

  def self.headers(sc_mc)
    h = HTTP::Headers.new
    h.add("Accept", "application/json")
    h.add("Content-Type", "application/json")
    h.add("Authorization", "Bearer " + api_key(sc_mc))
    h
  end

  def self.choose(vals : Array(String))
    vals[Random.rand(vals.size)]
  end

  class ChargeDetail
    include JSON::Serializable
    property sku : String = "12345"
    property description : String = "Blah"
    property quantity : Int32 = Random.rand(5) + 1
    property unit_price : Int32 = Random.rand(400) + 100
    property tax_amount : Int32 = 0
    property subtotal : Int32

    def initialize
      @subtotal = @quantity * @unit_price
    end
  end

  class Charge
    include JSON::Serializable
    property seller_id : String
    property buyer_id : String
    property currency : String
    property tax_amount : Int32 = 0
    property shipping_amount : Int32 = 0
    property order_url : String = "https://test.com/123"
    property order_number = "abc123"
    property po_number = "po123"
    property total_amount : Int32
    property details : Array(ChargeDetail)

    def initialize(sc_mc)
      client = CLIENTS[sc_mc]
      @seller_id = client.seller
      @buyer_id = BCaaS.choose(client.buyers)
      @currency = BCaaS.choose(client.currencies)
      @total_amount = 0
      @details = [] of ChargeDetail
      (Random.rand(5) + 1).times do |i|
        dtl = ChargeDetail.new
        @total_amount += dtl.subtotal
        @details << dtl
      end
    end
  end

  class PreAuth
    include JSON::Serializable
    property seller_id : String
    property buyer_id : String
    property currency : String
    property preauthorized_amount : Int32

    def initialize(sc_mc)
      client = CLIENTS[sc_mc]
      @seller_id = client.seller
      @buyer_id = BCaaS.choose(client.buyers)
      @currency = BCaaS.choose(client.currencies)
      @preauthorized_amount = Random.rand(4000) + 1000
    end
  end

  abstract class BCaaSTask < Beetle::Task
    def default_host
      BCaaS::HOST
    end

    def sc_mc
      ret = "mc"
      if p = @params
        if v = p["sc_mc"]?
          v = v.downcase
          v = "sc" unless v == "mc"
          ret = v
        end
      end
      ret
    end
  end

  class ChargeTask < BCaaSTask
    def submit(client : HTTP::Client)
      payload = Charge.new(sc_mc)
      @request_context = "buyer_id: #{payload.buyer_id}"
      @logger.warn("[#{@swarm_id}] #{@request_context}")
      client.post("/charges", headers: BCaaS.headers(sc_mc), body: payload.to_json.to_s)
    end
  end

  class PreAuthTask < BCaaSTask
    def submit(client : HTTP::Client)
      payload = PreAuth.new(sc_mc)
      @request_context = "buyer_id: #{payload.buyer_id}"
      @logger.warn("[#{@swarm_id}] #{@request_context}")
      client.post("/preauthorizations", headers: BCaaS.headers(sc_mc), body: payload.to_json.to_s)
    end
  end
end
