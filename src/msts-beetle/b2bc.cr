require "base64"

module B2BC
  extend self

  class Client
    property currencies : Array(String)
    property seller : String
    property buyers : Array(String)

    def initialize(@currencies, @seller, @buyers)
    end
  end

  HOST    = "https://api.staging.b2bcreditonline.com/v2"
  API_KEY = {
    "mc" => "3b7696ec-89e7-4836-aed9-0c4f3ca7fab5",
  }
  CLIENTS = {
    "mc" => Client.new(
      %w(EUR USD SGD GBP),
      "9da16652-df01-4a9c-9e03-c5bde8263896",
      [
        "23546ddc-cb7d-4a98-8895-5743a654e8d3",
        "86d6b35d-9237-4650-8daa-ac27ddd6cc8d",
        "b530f633-f70b-4487-abbb-e01cda1bb090",
        "6dd7a11a-c7b8-4a07-92fc-3c3e36d37a3e",
        "14e3f8b1-b841-4c0a-99d4-cee60c418a77",
        "ef173097-92d5-4057-8582-1b7de8ed6ec9",
        "0d65b7a7-f730-4a6c-b9d7-0da4409a9b28",
        "b1abcca4-75ee-4c74-936b-cf050fdff66e",
        "8dc50519-f6cf-47f4-a4ea-d68530458f7e",
        "ae832bbf-d6e3-4654-971d-25326915b8cb",
        "fca59873-b285-414b-922f-de0c651ff0bf",
        "f9d638b6-2fac-483c-9c22-287842a2cfec",
        "0304f20f-8af9-4d8e-a4c4-96deec0f38bd",
        "3946dd51-ec6f-41ee-808c-ae98a5da39b5",
        "db2b7e65-96ac-41c4-bedf-d0ec6c6e3243",
        "4474fb0d-20c4-4956-8023-ed691b2dcd9a",
        "1d0c5971-3e4f-41a1-bb7a-7f27b51d32af",
        "f29babd4-5683-446f-81a5-d85ef32d0bf5",
        "ae1736f8-515b-434a-a2e0-ba2d6ac918c7",
        "c1d11bb6-ba8e-4e3f-9611-09366781dd1c",
      ]
    ),
  }

  def api_key(sc_mc)
    API_KEY[sc_mc]
  end

  def self.headers(sc_mc)
    h = HTTP::Headers.new
    h.add("Accept", "application/json")
    h.add("Content-Type", "application/json")
    h.add("Authorization", "Basic " + Base64.strict_encode(api_key(sc_mc) + ":"))
    h
  end

  def self.choose(vals : Array(String))
    vals[Random.rand(vals.size)]
  end

  class InvoiceDetail
    include JSON::Serializable
    property sku : String = "12345"
    property description : String = "Blah"
    property quantity : Int32 = Random.rand(5) + 1
    property unit_price : Int32 = Random.rand(400) + 100
    property tax_amount : Int32 = 0
    property subtotal : Int32

    def initialize
      @subtotal = @quantity * @unit_price
    end
  end

  class Invoice
    include JSON::Serializable
    property client_reference_id : String = UUID.random.to_s
    property buyer_id : String
    property seller_id : String
    property date : String = Time.now.to_s("%F")
    property total : Int32
    property tax_amount : Int32 = 0
    property shipping_amount : Int32 = 0
    property seller_invoice_number : String = "DoofusWasHere"
    property po_number = "po123"
    property preauthorization_id : String? = nil
    property currency : String
    property details : Array(InvoiceDetail)

    def initialize(sc_mc)
      client = CLIENTS[sc_mc]
      @buyer_id = B2BC.choose(client.buyers)
      @seller_id = client.seller
      @total = 0
      @currency = B2BC.choose(client.currencies)
      @details = [] of InvoiceDetail
      (Random.rand(5) + 1).times do |i|
        dtl = InvoiceDetail.new
        @total += dtl.subtotal
        @details << dtl
      end
    end
  end

  class CurrencyAmount
    include JSON::Serializable
    property amount : Int32
    property currency : String
  end

  class PreAuthResponse
    include JSON::Serializable
    property id : String
    property buyer_id : String
    property seller_id : String
    property status : String
    property preauthorized_amount : Array(CurrencyAmount)
    property captured_amount : Array(CurrencyAmount)
    property foreign_exchange_fee : Array(CurrencyAmount)?
    property expires : String
    property created : String
    property updated : String
  end

  class PreAuth
    include JSON::Serializable
    property buyer_id : String
    property seller_id : String
    property preauthorized_amount : Int32
    property currency : String

    def initialize(sc_mc)
      client = CLIENTS[sc_mc]
      @buyer_id = B2BC.choose(client.buyers)
      @seller_id = client.seller
      @preauthorized_amount = Random.rand(4000) + 1000
      @currency = B2BC.choose(client.currencies)
    end

    def initialize(invoice : Invoice)
      @buyer_id = invoice.buyer_id
      @seller_id = invoice.seller_id
      @preauthorized_amount = invoice.total
      @currency = invoice.currency
    end
  end

  abstract class B2BCTask < Beetle::Task
    def default_host
      B2BC::HOST
    end

    def sc_mc
      "mc" # For now
    end
  end

  class InvoiceTask < B2BCTask
    def submit(client : HTTP::Client)
      payload = Invoice.new(sc_mc)
      @request_context = "buyer_id: #{payload.buyer_id}"
      @logger.warn("[#{@swarm_id}] #{@request_context}")
      client.post("/invoices", headers: B2BC.headers(sc_mc), body: payload.to_json.to_s)
    end
  end

  class PreAuthTask < B2BCTask
    def submit(client : HTTP::Client)
      payload = PreAuth.new(sc_mc)
      @request_context = "buyer_id: #{payload.buyer_id}"
      @logger.warn("[#{@swarm_id}] #{@request_context}")
      client.post("/preauthorizations", headers: B2BC.headers(sc_mc), body: payload.to_json.to_s)
    end
  end

  class PreAuthInvoiceTask < B2BCTask
    def submit(client : HTTP::Client)
      payload_i = Invoice.new(sc_mc)
      payload_p = PreAuth.new(payload_i)
      @request_context = "buyer_id: #{payload_i.buyer_id}"
      @logger.warn("[#{@swarm_id}] #{@request_context}")
      resp = client.post("/preauthorizations", headers: B2BC.headers(sc_mc), body: payload_p.to_json.to_s)
      if resp.success?
        if resp.headers["Content-Type"].downcase.starts_with?("application/json")
          preauth = PreAuthResponse.from_json(resp.body.not_nil!)
          if preauth.status == "preauthorized"
            payload_i.preauthorization_id = preauth.id
            resp = client.post("/invoices", headers: B2BC.headers(sc_mc), body: payload_i.to_json.to_s)
          end
        end
      end
      resp
    end
  end
end
