# AWS Cheat Sheet

## Check everything build ok

```bash
shards build
```

## Increment the `tag` in ~/helm/aws-deploy.yaml

## Build and deploy the docker image (nnnn = the value of `tag`)

```bash
docker build . -t msts/bcaas-beetle -t 434875166128.dkr.ecr.us-east-1.amazonaws.com/msts/bcaas-beetle:nnnn
aws_session
$(aws ecr get-login --no-include-email --region us-east-1)
docker push 434875166128.dkr.ecr.us-east-1.amazonaws.com/msts/bcaas-beetle:nnnn
```

## Tear down the current deployment

```bash
helm delete numpty --purge
```

## Re-install (make sure you're in the ~/helm directory)

```bash
helm install . -n numpty -f aws-deploy.yaml --debug
helm status numpty
```